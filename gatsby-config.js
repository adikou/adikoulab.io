/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    siteTitle: `@adikou's Personal Musings`,
    siteDescription: `A collection of my thoughts on life, tech, and other nonsense`,
    siteLanguage: `en`,
    author: `adikou`,
    social: {
      twitter: {
        handle: `@libncurses`,
        url: `https://twitter.com/libncurses`,
      },
    },
  },
  /* Your site config here */
  plugins: [
    {
      resolve: `@lekoarts/gatsby-theme-minimal-blog`,
      options: {
        navigation: [
          {
            title: `Blog`,
            slug: `/blog`,
          },
          {
            title: `About`,
            slug: `/about`,
          },
        ],
        externalLinks: [
          {
            name: `Twitter`,
            url: `https://twitter.com/libncurses`,
          },
          {
            name: `Instagram`,
            url: `https://www.instagram.com/tangospotted`,
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `@adikou's Personal Musings`,
        short_name: `@adikou`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#6B46C1`,
        display: `standalone`,
        icons: [
          {
            src: `/android-chrome-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `/android-chrome-512x512.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
        ],
      },
    },
    `gatsby-plugin-offline`,
  ],
}
