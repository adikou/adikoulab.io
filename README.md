# adikou.gitlab.io

Project repo to house my personal website, hosted on GitLab Pages.
I will be experimenting with various static site generators such as Gatsby, Jekyll, Hugo, and plain HTML/CSS.
The base template for the TypeScript is from [using-typescript](https://github.com/gatsbyjs/gatsby/tree/master/examples/using-typescript).

## Legal Disclaimer
This project is not a public contribution and not affiliated with my employer(s), past or present. 
Commits/Contributions/Development to this project have been made from personal laptop computers
and not from any proprietary electronic devices.
             